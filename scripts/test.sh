function validateError(){
	findErrors=$(awk '/Errors/{print NR}' $salidaCompilacion)
	if [ ! -z $findErrors ]
	then
        echo " "
        cat $salidaCompilacion
        rm $salidaCompilacion 
        echo " "                           
        echo -e "$COL_RED Maquinola cosmica, no se pudieron restaurar los Nuget $COL_RESET"
        exit -1
    fi
    echo -e "$COL_GREEN [Listorti] ^_^ $COL_RESET"
    rm $salidaCompilacion
}

salidaCompilacion=test.log

$((1/0)) > $salidaCompilacion
validateError $salidaCompilacion

exit 0